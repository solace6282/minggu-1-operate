package com.example.minggu1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var op1 : Double = 0.0
    var op2 : Double = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        button_div.setOnClickListener {
            getOp()
            if (op2 == 0.0) {
                result.text = "Tidak bisa dilakukan"
            } else {
                result.text = (op1 / op2).toString()
            }
        }

        button_plus.setOnClickListener {
            getOp()
            result.text = (op1 + op2).toString()
        }

        button_min.setOnClickListener {
            getOp()
            result.text = (op1 - op2).toString()
        }

        button_mul.setOnClickListener {
            getOp()
            result.text = (op1 * op2).toString()
        }
    }

    fun getOp(){
        op1 = op1text.text.toString().toDouble()
        op2 = op2text.text.toString().toDouble()
    }
}